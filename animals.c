///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   28 January 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   // @todo Map the enum Color to a string
      switch(color){
         case 0:
            return "Black";
         case 1:
            return "White";
         case 2:
            return "Red";
         case 3:
            return "Blue";
         case 4:
            return "Green";
         case 5:
            return "Pink";
         default: return NULL;
      }

}

//   return NULL; // We should never get here
// };

char* genderName (enum Gender gen) {


      switch(gen){
         case 0:
            return "Male";
         case 1:
            return "Female";
         default: return NULL;
      }
}

char* breedName (enum CatBreeds breed) {


      switch(breed){
         case 0:
            return "Main Coon";
         case 1:
            return "Manx";
         case 2:
            return "Shorthair";
         case 3:
            return "Persain";
         case 4:
            return "Sphynx";
         default: return NULL;
      }
}

char* yesno (bool fixed){
      if(fixed == 1)
         return "Yes";
      else
         return "No";

}


